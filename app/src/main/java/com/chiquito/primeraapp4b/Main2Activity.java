package com.chiquito.primeraapp4b;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener, frg1.OnFragmentInteractionListener,Frg2.OnFragmentInteractionListener{

    Button buttonFrg1, buttonFrg2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        buttonFrg1 =(Button) findViewById(R.id.buttonfgm1);
        buttonFrg2 =(Button) findViewById(R.id.buttonfgm2);
        buttonFrg1.setOnClickListener(this);
        buttonFrg2.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonfgm1:
                frg1 fragmentoUno = new frg1();
                FragmentTransaction transaccionUno = getSupportFragmentManager().beginTransaction();
                transaccionUno.replace(R.id.contenedor, fragmentoUno);
                transaccionUno.commit();
                break;
            case R.id.buttonfgm2:
                Frg2 fragmentoDos = new Frg2();
                FragmentTransaction transaccionDos = getSupportFragmentManager().beginTransaction();
                transaccionDos.replace(R.id.contenedor, fragmentoDos);
                transaccionDos.commit();
                break;
        }
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
