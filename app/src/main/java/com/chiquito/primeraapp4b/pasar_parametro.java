package com.chiquito.primeraapp4b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class pasar_parametro extends AppCompatActivity {

    EditText cajaDatos;
    Button buttonEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);
        cajaDatos = (EditText) findViewById(R.id.txtParametro);
        buttonEnviar = (Button) findViewById(R.id.buttonEnviarParametro);
        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(pasar_parametro.this, recibir_parametro.class);
                // se crea un onjeto de tipo bundle que será la bitacora de parametros a enviar
                Bundle bundle = new Bundle();
                bundle.putString("dato", cajaDatos.getText().toString());
                //metodo putExtras envia un objeto de tipo bundle como un solo parametro entre actividades
                intent.putExtras(bundle);
                startActivity(intent);
            }

        });

    }
}
